output "EKS" {
  value = <<EOF

>> Environment <<
- NAME                  :: ${local.environment_name}
- ID                    :: ${module.argo_platform.networking_vpc_id}
- REGION                :: ${var.AWS_REGION}
- Availability Zones    :: ${join(", ", module.argo_platform.networking_application_availability_zones)}

>> Networking <<
- CIDR                  :: ${var.VPC_CIDR_BLOCK}
- Public                :: ${join(", ", module.argo_platform.networking_public_subnets)}
- Application           :: ${join(", ", module.argo_platform.networking_application_subnets)}

>> External Access <<
- Ingress DNS             :: ${module.argo_platform.ingress_alb_dns_name}

Cluster endpoint: ${module.argo_platform.eks_endpoint}

Interact with created cluster:
kubectl --kubeconfig ${module.argo_platform.eks_kubeconfig_filename} get pods --all-namespaces

List cluster nodes:
kubectl --kubeconfig ${module.argo_platform.eks_kubeconfig_filename} get nodes

Copy kubeconfig to your home
cp kubeconfig.conf ~/.kube/config

>> ArgoCD variables <<

Release metadata name             :: ${module.argo_platform.argocd_name}
Release metadata chart_name       :: ${module.argo_platform.argocd_chart_name}
Release metadata revision         :: ${module.argo_platform.argocd_revision}
Release metadata namespace        :: ${module.argo_platform.argocd_namespace}
Release metadata version          :: ${module.argo_platform.argocd_version}
Release metadata app_version      :: ${module.argo_platform.argocd_app_version}

>> ArgoCD usage <<
Argo should be exposed via nginx ingress on argo.${var.DNS_ZONE_NAME}. 
Default password is stored in ArgoCD module.

If you want to use ArgoCd with Idemia Artifactory helm charts repository.

1. Install argocd client
https://argoproj.github.io/argo-cd/cli_installation/

2. Logging to Argo
argocd login argo.${var.DNS_ZONE_NAME}:443 --grpc-web-root-path / --username user --name user  --password 'strong-password'

3. Add repository template
argocd repocreds add https://docker-release.otlabs.fr/artifactory --username 'artifactory-user' --password 'strong-artifactory-password'

or do everything via web browser ¯\_(ツ)_/¯
EOF
}