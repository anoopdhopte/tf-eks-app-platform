# Locale variables
locals {
  environment_name = "${module.argo_platform.common_globals["Project"]}-${module.argo_platform.common_globals["Environment"]}"
}

variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
}

variable "PROJECT" {
  description = "Provide name of the project"
}

variable "ENVIRONMENT" {
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
  default     = "testing"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks per availability zones, ex azX = '10.1.20.0/24'"
  type        = list(string)
}

variable "DNS_ZONE_NAME" {
  description = "Provide dns zone name"
}

variable "KUBERNETES_VERSION" {
  description = "Version of kubernetes which should be deployed"
  type        = string
}

variable "OFFICE_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of lodz office"
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
  type        = list(string)
}

variable "CI_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of CI"
  default     = ["18.192.228.102/32", "18.158.189.113/32", "18.192.153.221/32", "18.193.144.231/32"]
  type        = list(string)
}

variable "EXTRA_CIDR_BLOCKS" {
  description = "Provide extra CIDR blocks"
  default     = []
  type        = list(string)
}

variable "ADMIN_ROLES_ARN" {
  description = "AWS roles which have access to kubernetes API"
  type        = list(string)
}

variable "PROMETHEUS_EXTERNAL_LABEL" {
  description = "Prometheus label used to thanos connection"
  type        = string
}

variable "INSTALL_DEFAULT_APPS" {
  type        = bool
  default     = true
  description = "If true platform will install default applications like extrnal-dns/kong-ingress/istio/kubernetes-autoscaller..."
}

variable "COGNITO_API_KEY" {
  type        = string
  default     = ""
  description = "App api key for integration with corporate cognito"
}

variable "COGNITO_API_SECRET" {
  type        = string
  default     = ""
  description = "App api key secret for integration with corporate cognito"
}

variable "FEDERATED_COGNITO_DOMAIN" {
  type        = string
  default     = ""
  description = "Federated cognito public url"
}

variable "USER_POOL_ID" {
  type        = string
  default     = ""
  description = "User pool id"
}

variable "ISSUER_URL" {
  type        = string
  default     = ""
  description = "Issuer url"
}

variable "ELASTICSEARCH_LOGGING_ENDPOINT" {
  description = "ES endpoint where logs will be pushed"
  default     = ""
  type        = string
}

variable "ELASTICSEARCH_LOGGING_REGION" {
  description = "ES region"
  default     = ""
  type        = string
}

variable "OBSERVABILITY_LOGGING_ROLE" {
  description = "Logging observabitlity IAM role arn"
  default     = ""
  type        = string
}

variable "FLUENTD_REPLICAS" {
  description = "Number of replicas of fluentd"
  default     = 3
  type        = number
}

variable "DOCKER_REGISTRY_CREDENTIALS" {
  description = "Provide list of docker registries with credentials."
  default     = []
  type        = list(string)
}

variable "AWS_LOGGING_ENABLED" {
  description = "Enable logging for all AWS components"
  type        = bool
  default     = true
}

variable "APP_NR_OF_NODES" {
  description = "Provide number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "APP_MIN_NR_OF_NODES" {
  description = "Provide MIN number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "APP_MAX_NR_OF_NODES" {
  description = "Provide MAX number of kubernetes nodes"
  default     = 10
  type        = number
}

variable "MANAGEMENT_NR_OF_NODES" {
  description = "Provide number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "MANAGEMENT_MIN_NR_OF_NODES" {
  description = "Provide MIN number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "MANAGEMENT_MAX_NR_OF_NODES" {
  description = "Provide MAX number of kubernetes nodes"
  default     = 5
  type        = number
}
