provider "aws" {
  region = "eu-central-1"
}

terraform {
  required_version = "~> 0.14.8"
}

module "argo_platform" {
  source                         = "../../../modules/platform"
  AWS_REGION                     = var.AWS_REGION
  PROJECT                        = var.PROJECT
  ENVIRONMENT                    = var.ENVIRONMENT
  ACCOUNT_NAME                   = var.ACCOUNT_NAME
  VPC_CIDR_BLOCK                 = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS             = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS        = var.APPLICATION_CIDR_BLOCKS
  DNS_ZONE_NAME                  = var.DNS_ZONE_NAME
  KUBERNETES_VERSION             = var.KUBERNETES_VERSION
  ADMIN_ROLES_ARN                = var.ADMIN_ROLES_ARN
  PROMETHEUS_EXTERNAL_LABEL      = var.PROMETHEUS_EXTERNAL_LABEL
  COGNITO_API_KEY                = var.COGNITO_API_KEY
  COGNITO_API_SECRET             = var.COGNITO_API_SECRET
  FEDERATED_COGNITO_DOMAIN       = var.FEDERATED_COGNITO_DOMAIN
  USER_POOL_ID                   = var.USER_POOL_ID
  ISSUER_URL                     = var.ISSUER_URL
  ELASTICSEARCH_LOGGING_ENDPOINT = var.ELASTICSEARCH_LOGGING_ENDPOINT
  ELASTICSEARCH_LOGGING_REGION   = var.ELASTICSEARCH_LOGGING_REGION
  OBSERVABILITY_LOGGING_ROLE     = var.OBSERVABILITY_LOGGING_ROLE
  FLUENTD_REPLICAS               = var.FLUENTD_REPLICAS
  APP_NR_OF_NODES                = var.APP_NR_OF_NODES
  APP_MIN_NR_OF_NODES            = var.APP_MIN_NR_OF_NODES
  APP_MAX_NR_OF_NODES            = var.APP_MAX_NR_OF_NODES
  MANAGEMENT_NR_OF_NODES         = var.MANAGEMENT_NR_OF_NODES
  MANAGEMENT_MIN_NR_OF_NODES     = var.MANAGEMENT_MIN_NR_OF_NODES
  MANAGEMENT_MAX_NR_OF_NODES     = var.MANAGEMENT_MAX_NR_OF_NODES
  INSTALL_CLUSTER_AUTOSCALER     = var.INSTALL_CLUSTER_AUTOSCALER
  INSTALL_EXTERNAL_DNS           = var.INSTALL_EXTERNAL_DNS
  INSTALL_HERMES                 = var.INSTALL_HERMES
  INSTALL_ISTIO                  = var.INSTALL_ISTIO
  INSTALL_JAEGER                 = var.INSTALL_JAEGER
  INSTALL_KIALI                  = var.INSTALL_KIALI
  INSTALL_KONG_INGRESS           = var.INSTALL_KONG_INGRESS
  INSTALL_KUBE2IAM               = var.INSTALL_KUBE2IAM
  INSTALL_KUBED                  = var.INSTALL_KUBED
  INSTALL_LOGGING                = var.INSTALL_LOGGING
  INSTALL_SCHEMA_REGISTRY_UI     = var.INSTALL_SCHEMA_REGISTRY_UI
  INSTALL_VAULT                  = var.INSTALL_VAULT
  AWS_LOGGING_ENABLED            = var.AWS_LOGGING_ENABLED
  DOCKER_REGISTRY_CREDENTIALS    = var.DOCKER_REGISTRY_CREDENTIALS
}

data "aws_route53_zone" "main" {
  name         = "cat-test.idemia.io"
  private_zone = false
}

module "delegation" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/delegation?ref=1.1.0"
  NAME_SERVERS  = module.argo_platform.dns_public_name_servers
  DNS_ZONE_NAME = module.argo_platform.dns_zone_name
  ZONE_ID       = data.aws_route53_zone.main.id
}
