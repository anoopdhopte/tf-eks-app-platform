resource "aws_security_group_rule" "oneplatform_database_egress_3306" {
  description       = "Allow outgoing network flow on port 3306/tcp - mysql"
  type              = "egress"
  from_port         = 3306
  to_port           = 3306
  protocol          = "tcp"
  cidr_blocks       = var.DATABASE_CIDR_BLOCK
  security_group_id = aws_security_group.oneplatform_node.id
}

resource "aws_security_group_rule" "oneplatform_msk_client_egress_2181_2182" {
  description       = "Allow outgoing network flow on port 2181/tcp, 2182/tcp - zookeeper"
  type              = "egress"
  from_port         = 2181
  to_port           = 2182
  protocol          = "tcp"
  cidr_blocks       = [var.VPC_CIDR_BLOCK]
  security_group_id = aws_security_group.oneplatform_node.id
}

resource "aws_security_group_rule" "oneplatform_msk_client_egress_9092" {
  description       = "Allow outgoing network flow on port 9092/tcp - kafka"
  type              = "egress"
  from_port         = 9092
  to_port           = 9092
  protocol          = "tcp"
  cidr_blocks       = [var.VPC_CIDR_BLOCK]
  security_group_id = aws_security_group.oneplatform_node.id
}

resource "aws_security_group_rule" "oneplatform_msk_client_egress_9094" {
  description       = "Allow outgoing network flow on port 9094/tcp - kafka"
  type              = "egress"
  from_port         = 9094
  to_port           = 9094
  protocol          = "tcp"
  cidr_blocks       = [var.VPC_CIDR_BLOCK]
  security_group_id = aws_security_group.oneplatform_node.id
}

resource "aws_security_group_rule" "oneplatform_database_egress_9042" {
  description       = "Allow outgoing network flow on port 9042"
  type              = "egress"
  from_port         = 9042
  to_port           = 9042
  protocol          = "tcp"
  cidr_blocks       = var.DATABASE_CIDR_BLOCK
  security_group_id = aws_security_group.oneplatform_node.id
}

resource "aws_security_group_rule" "database_egress_5432" {
  description       = "Allow outgoing network flow on port 5432/tcp - aurora-pg"
  type              = "egress"
  from_port         = 5432
  to_port           = 5432
  protocol          = "tcp"
  cidr_blocks       = var.DATABASE_CIDR_BLOCK
  security_group_id = aws_security_group.oneplatform_node.id
}

resource "aws_security_group_rule" "database_egress_6379" {
  description       = "Allow outgoing network flow on port 6379/tcp - redis"
  type              = "egress"
  from_port         = 6379
  to_port           = 6379
  protocol          = "tcp"
  cidr_blocks       = var.DATABASE_CIDR_BLOCK
  security_group_id = aws_security_group.oneplatform_node.id
}
