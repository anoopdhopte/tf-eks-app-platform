module "msk" {
  source              = "git::https://bitbucket.org/anoopdhopte/msk.git//modules?ref=1.0.3"
  globals             = module.common.globals
  networking          = module.networking.common
  NAME                = "msk"
  SUBNET_IDS          = module.networking.application_subnet_ids
  INGRESS_ACCESS_CIDR = compact(concat([var.VPC_CIDR_BLOCK], var.EXTRA_MSK_INGRESS_ACCESS_CIDRS))
  # MSK_DEFAULT_REPLICATION_FACTOR = 1
  MSK_EBS_VOLUME_SIZE            = 50
  MSK_ENCRYPTION_DATA_IN_TRANSIT = var.MSK_PROTOCOL
  MSK_INSTANCE_TYPE              = "kafka.t3.small"
  MSK_KAFKA_VERSION              = "2.2.1"
  MSK_CLOUDWATCH_LOG_GROUP_NAME  = var.AWS_LOGGING_ENABLED == true ? module.log_group.log_group_name : ""
  depends_on                     = [module.networking]
}
