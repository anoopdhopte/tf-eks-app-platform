resource "aws_security_group" "oneplatform_node" {
  name        = "${local.environment_name}-oneplatform-node-sg"
  description = "Allow to use MSK"
  vpc_id      = module.networking.vpc_id

  tags = merge(
    map(
      "CreatedBy", "terraform",
      "Name", "${local.environment_name}-default-security-group",
      "Object", "aws_security_group"
    )
  )
}